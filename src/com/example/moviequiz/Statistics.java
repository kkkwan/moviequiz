package com.example.moviequiz;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Statistics extends Activity {
		
	private Button backButton;
	private TextView quizzesTaken;
	private TextView correct;
	private TextView incorrect;
	private TextView totalTime;
	private TextView totalQ;
	private TextView timePerQ;
	private Button resetButton;
	
	private DbAdapter db;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics);

        TextView tv = (TextView)this.findViewById(R.id.message);
        tv.setText("Your Overall Performance");
        
		quizzesTaken = (TextView)this.findViewById(R.id.quizzesTaken);
		correct = (TextView)this.findViewById(R.id.correct);
		incorrect = (TextView)this.findViewById(R.id.incorrect);
		totalTime = (TextView)this.findViewById(R.id.totalTime);
		totalQ = (TextView)this.findViewById(R.id.totalQ);
		timePerQ = (TextView)this.findViewById(R.id.timePerQ);
//		resetButton = (Button)this.findViewById(R.id.reset);
        
//        // Retrieve the button, change its value and add an event listener
//        this.backButton = (Button)this.findViewById(R.id.backButton);
//        this.backButton.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				finish();
//			}
//        });
        
//		this.resetButton.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				db.clearScores();
//				Toast toast = Toast.makeText(Statistics.this, "Profile has been reset.", 3);
//				toast.show();
//				finish();
//			}
//		});
		
		db = new DbAdapter(this);
		Cursor cur = db.getStats();
		boolean hasData = cur.moveToFirst();
		
//		display information
		if(hasData){
			quizzesTaken.setText("Number of Quizzes Taken:                  " + db.getTotalNumQuizzes());
			correct.setText("Questions Answered Correctly:          " + db.getTotalNumCorrect());
			incorrect.setText("Questions Answered Incorrectly:       " + db.getTotalNumIncorrect());
			totalTime.setText("Total Play Time:                                    " + db.getTotalTime());
			
			int totalNumQ = db.getTotalNumCorrect() + db.getTotalNumIncorrect();
			totalQ.setText("Total Questions Attempted:                " + totalNumQ);
			
//			"Number of Quizzes Taken:                  None" 
//			"Questions Answered Correctly:          No games played." 
//			"Questions Answered Incorrectly:       No games played." 
//			"Total Questions Attempted:                No games played." 
//			"Total Play Time:                                    No games played." 
//			"Average Time Per Question:               No games played." 
			
			double avgTimePerQ;
//			if (totalNumQ == 0)
//				avgTimePerQ = 0;
//			else
//				avgTimePerQ = (db.getTotalTime() / totalNumQ);
			avgTimePerQ = (db.getTotalTime() / totalNumQ);
			DecimalFormat f = new DecimalFormat();
			f.setMinimumFractionDigits(3);
			timePerQ.setText("Average Time Per Question:               " + f.format(avgTimePerQ) + " seconds");
		}
		
		db.close();
    }
}