package com.example.moviequiz;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class End extends Activity {
		
	private Button backButton;
	private TextView quizzesTaken;
	private TextView correct;
	private TextView incorrect;
	private TextView totalTime;
	private TextView totalQ;
	private TextView timePerQ;
	private Button resetButton;
	private DbAdapter db;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end);

        TextView tv = (TextView)this.findViewById(R.id.message);
        tv.setText("Game Over!");
        
		this.correct = (TextView)this.findViewById(R.id.correct);
		this.incorrect = (TextView)this.findViewById(R.id.incorrect);
		this.totalTime = (TextView)this.findViewById(R.id.totalTime);
		this.totalQ = (TextView)this.findViewById(R.id.totalQ);
		this.timePerQ = (TextView)this.findViewById(R.id.timePerQ);        
        
        // Retrieve the button, change its value and add an event listener
//        this.backButton = (Button)this.findViewById(R.id.backButton);
//        this.backButton.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				finish();
//			}
//        });
        
        int correctAmt = getIntent().getExtras().getInt("correct");
		int incorrectAmt = getIntent().getExtras().getInt("totalQ") - correctAmt;
		int totalQAmt= getIntent().getExtras().getInt("totalQ");
		
		TextView debug = (TextView)this.findViewById(R.id.debug);
        debug.setText("");
//        debug.setText(String.valueOf(totalQAmt));
//        debug.setText(String.valueOf(Quiz.numCorrect) + "..." + String.valueOf(Quiz.numQ));
//        debug.setText(String.valueOf(totalQAmt));
        
		this.correct.setText("Questions Answered Correctly:          " + correctAmt);
		this.incorrect.setText("Questions Answered Incorrectly:       " + incorrectAmt);
		this.totalQ.setText("Total Questions Attempted:                " + totalQAmt);

		Long totalSeconds = (long) Quiz.duration / 1000;
		Long totalDecimalSeconds = (long) 0;
		if(getIntent().getExtras().getInt("totalQ") != 0){
			Long totalMilliseconds = getIntent().getExtras().getLong("totalTime");
			Long averageMilliseconds = totalMilliseconds / getIntent().getExtras().getInt("totalQ");
			totalSeconds = averageMilliseconds / 1000;
			totalDecimalSeconds = averageMilliseconds % 1000;
		}
		
		double timePerQ = Double.parseDouble(totalSeconds + "." + totalDecimalSeconds);
		this.timePerQ.setText("Average Time Per Question:                " + timePerQ + " seconds");
		this.totalTime.setText("Total Play Time:                                    " + (getIntent().getExtras().getLong("totalTime") / 1000) + " seconds");
		
//        debug.setText(String.valueOf(totalQAmt));

		db = new DbAdapter(this);
		db.insertNewStats(correctAmt, incorrectAmt, totalSeconds);
    } //onCreate
}