package com.example.moviequiz;

import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Quiz extends Activity {
	
	private Button backButton;
	private Button ans1;
	private Button ans2;
	private Button ans3;
	private Button ans4;
	private boolean chosen = false;		
	
	private TextView mTimeLabel;
	private Handler mHandler = new Handler();
	private long mStart;
	private long mPause = 0;
	public static long duration = 180000; //three minutes 180000
	
	private Random rand = new Random();
	private int qNum;
	
	private String correctAns;
	private DbAdapter db;
	private Cursor cur;

	long now = 0;
	private long elapsed = duration;
	
	private boolean stop = false;;
	
	public static int numCorrect = 0;
	public static int numQ = 0;
	
	private boolean getIncorrectAns = true;
	private boolean end = false;
	
	private Runnable updateTask = new Runnable() {	
		public void run() {
				now = SystemClock.uptimeMillis();
				elapsed = duration - (now - mStart);
				
				if (elapsed > 0) {
					int seconds = (int) (elapsed / 1000);
					int minutes = seconds / 60;
					seconds     = seconds % 60;
	
					if (seconds < 10) {
						mTimeLabel.setText("" + minutes + ":0" + seconds);
					} else {
						mTimeLabel.setText("" + minutes + ":" + seconds);            
					}
	
					mHandler.postAtTime(this, now + 1000);
				}
				else {
					mHandler.removeCallbacks(this);
					mHandler.removeCallbacks(updateTask);
					Intent intent = new Intent(Quiz.this, End.class);
					intent.putExtra("correct", numCorrect);
					intent.putExtra("totalQ", numQ);
					intent.putExtra("totalTime", duration);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					
					if (!stop)
						startActivity(intent);
					finish();
					
				}
		}
		
	};
	

	public void genQ(final Bundle savedInstanceState){

        TextView tv = (TextView)this.findViewById(R.id.question);
        tv.setText("Loading Question...");

		qNum = rand.nextInt(8);


        db = new DbAdapter(this);
        Question question = db.getQuestion(qNum);
        db.close();

		ArrayList<Answer> answers = question.getAnswers();

        //get our question string
        tv.setText(question.getString());

        //populate our answers
        this.ans1.setText(answers.get(0).getString());
        this.ans2.setText(answers.get(1).getString());
        this.ans3.setText(answers.get(2).getString());
        this.ans4.setText(answers.get(3).getString());

        for (Answer ans : answers)
        {
            if (ans.isCorrect())
                correctAns = ans.getString();
        }

        
        this.ans1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!chosen){
					if (ans1.getText().equals(correctAns)){
						ans1.setBackgroundResource(R.drawable.btn_default_normal_green);
						numCorrect++;
					}
					else
						ans1.setBackgroundResource(R.drawable.btn_default_normal_red);
					chosen = true;
					numQ++;
			    	nextQuestion();
				}
			}
        });
        
        this.ans2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!chosen){
					if (ans2.getText().equals(correctAns)){
						ans2.setBackgroundResource(R.drawable.btn_default_normal_green);
						numCorrect++;
					}
					else
						ans2.setBackgroundResource(R.drawable.btn_default_normal_red);
					chosen = true;
					numQ++;
			    	nextQuestion();
			    }
			}
        });

        this.ans3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!chosen){
					if (ans3.getText().equals(correctAns)){
						ans3.setBackgroundResource(R.drawable.btn_default_normal_green);
						numCorrect++;
					}
					else
						ans3.setBackgroundResource(R.drawable.btn_default_normal_red);
					chosen = true;
					numQ++;
			    	nextQuestion();		
			    }
			}
        });
        
        this.ans4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!chosen){
					if (ans4.getText().equals(correctAns)){
						ans4.setBackgroundResource(R.drawable.btn_default_normal_green);
						numCorrect++;
					}
					else
						ans4.setBackgroundResource(R.drawable.btn_default_normal_red);
					chosen = true;
					numQ++;
			    	nextQuestion();
			    }
			}
        });
		
	}

	
    private void nextQuestion(){
    	mHandler.removeCallbacks(updateTask);
		Intent intent = new Intent(Quiz.this, Quiz.class);
		intent.putExtra("pauseTime", mStart);
		intent.putExtra("correct", numCorrect);
		intent.putExtra("totalQ", numQ);
		
		startActivity(intent);
		this.finish();
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz);
        
        stop = false;

        TextView tv = (TextView)this.findViewById(R.id.question);
        tv.setText("Hi");
        DbAdapter db = new DbAdapter(this);
        Cursor cur = db.fetchAll();
        
        TextView debug = (TextView)this.findViewById(R.id.debug);
        debug.setText("");
        
        this.ans1 = (Button)this.findViewById(R.id.answer1);
        this.ans2 = (Button)this.findViewById(R.id.answer2);
        this.ans3 = (Button)this.findViewById(R.id.answer3);
        this.ans4 = (Button)this.findViewById(R.id.answer4);
        mTimeLabel = (TextView)this.findViewById(R.id.timeLabel);

        long temp = (long)this.getIntent().getLongExtra("pauseTime",-1);
//        debug.setText(String.valueOf((long)this.getIntent().getLongExtra("pauseTime",-1)));
        	
        if (temp == -1)
        	mStart = SystemClock.uptimeMillis();
        else
        	mStart = temp;
                
        int temp2 = this.getIntent().getIntExtra("correct",-1);
        int temp3 = this.getIntent().getIntExtra("totalQ",-1);

        if (temp2 == -1)
        	numCorrect = 0;
        else
        	numCorrect = temp2;
        
        if (temp3 == -1)
        	numQ = 0;
        else
        	numQ = temp3;
        
//        debug.setText(String.valueOf(numCorrect) + "..." + String.valueOf(numQ - numCorrect));
        
        

        genQ(savedInstanceState);        
        
//        Retrieve the button, change its value and add an event listener
//        this.backButton = (Button)this.findViewById(R.id.backButton);
//        this.backButton.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				mHandler.removeCallbacks(updateTask);
//				stop = true;
//				finish();
//			}
//        });
        
        cur.close();
		db.close();
        
    }
    
    @Override
    public void onPause()
    {
    	super.onPause();
    	mPause = SystemClock.uptimeMillis();
        mHandler.removeCallbacks(updateTask);
    }
    
    @Override
    public void onResume()
    {
    	super.onResume();
    	if (mPause != 0)
    	{
    		mStart += (SystemClock.uptimeMillis()- mPause);
    		mPause = 0;
    	}
        mHandler.post(updateTask);
    }
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
    	super.onSaveInstanceState(savedInstanceState);
    	 savedInstanceState.putLong("pauseTime",SystemClock.uptimeMillis());
    	 savedInstanceState.putLong("savedMStart", mStart);
    	 savedInstanceState.putString("qText", (String) ((TextView)this.findViewById(R.id.question)).getText());
    	 savedInstanceState.putString("ans1", (String) ((Button)this.findViewById(R.id.answer1)).getText());
    	 savedInstanceState.putString("ans2", (String) ((Button)this.findViewById(R.id.answer2)).getText());
    	 savedInstanceState.putString("ans3", (String) ((Button)this.findViewById(R.id.answer3)).getText());
    	 savedInstanceState.putString("ans4", (String) ((Button)this.findViewById(R.id.answer4)).getText());
    	 savedInstanceState.putString("ans", correctAns);
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
    	super.onRestoreInstanceState(savedInstanceState);
    	
    	mPause = savedInstanceState.getLong("pauseTime");
    	mStart = savedInstanceState.getLong("savedMStart");
    	
    	mStart += (SystemClock.uptimeMillis() - mPause);

    	mPause = 0;
		((TextView)this.findViewById(R.id.question)).setText(savedInstanceState.getString("qText"));
		((Button)this.findViewById(R.id.answer1)).setText(savedInstanceState.getString("ans1"));
		((Button)this.findViewById(R.id.answer2)).setText(savedInstanceState.getString("ans2"));
		((Button)this.findViewById(R.id.answer3)).setText(savedInstanceState.getString("ans3"));
		((Button)this.findViewById(R.id.answer4)).setText(savedInstanceState.getString("ans4"));
		correctAns = savedInstanceState.getString("ans");
    }
    
    public void onBackPressed(){
    	moveTaskToBack(true);
    }
}