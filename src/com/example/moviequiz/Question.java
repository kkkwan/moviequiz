package com.example.moviequiz;

/**
 * Created by envison on 3/15/15.
 */
import com.example.moviequiz.Answer;

import java.util.ArrayList;

public class Question{
    private String questionString;
    private ArrayList<Answer> answerList;

    public Question()
    {
        this.questionString = "NULL";
        this.answerList = new ArrayList<Answer>();
    }

    public Question(String str, ArrayList<Answer> answers)
    {
        this.questionString = str;
        this.answerList = answers;
    }

    public void setAnswers(ArrayList<Answer> list) { this.answerList = list; }

    public ArrayList<Answer> getAnswers() { return this.answerList; }

    public void setString(String str) { this.questionString = str; }

    public String getString() { return this.questionString; }

}
