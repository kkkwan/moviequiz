package com.example.moviequiz;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.content.Intent;



public class Main extends Activity {

	private Button takeQuizButton;
	private Button statsButton;
	
	//Have two onClickListeners for the main view
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);
		
		this.takeQuizButton = (Button)this.findViewById(R.id.take_quiz);
		this.takeQuizButton.setOnClickListener(new OnClickListener() {
		   	public void onClick(View v) {
	    		Intent intent = new Intent(Main.this, Quiz.class);
	    		startActivity(intent);
	    		intent.putExtra("pauseTime", 0);
				intent.putExtra("correct", 0);
				intent.putExtra("totalQ", 0);
//	    		finish();
	    	}
	    });
		
		this.statsButton = (Button)this.findViewById(R.id.stats);
		this.statsButton.setOnClickListener(new OnClickListener() {
		   	public void onClick(View v) {
	    		Intent intent = new Intent(Main.this, Statistics.class);
	    		startActivity(intent);
	    		finish();
	    	}
	    });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
