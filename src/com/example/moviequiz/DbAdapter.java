package com.example.moviequiz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class DbAdapter extends SQLiteOpenHelper{

	private static final String DATABASE_NAME = "moviedb";
	private static final int DATABASE_VERSIon = 1;
	private SQLiteDatabase mDb;
	private Context mContext;
	private Random rand = new Random();
	
	public DbAdapter(Context ctx){
		super(ctx, DATABASE_NAME, null, DATABASE_VERSIon);
		mContext = ctx;
		this.mDb = getWritableDatabase();
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		String moviesTable = "CREATE TABLE movies (ID integer primary key autoincrement, TITLE text not null, YEAR text not null, DIRECTOR text not null);";
		String starsTable = "CREATE TABLE stars (ID integer primary key autoincrement, FIRST_NAME text not null, LAST_NAME text not null, DOB numeric not null, PHOTO_URL text);";
		String starsInMoviesTable = "CREATE TABLE stars_in_movies (STAR_ID integer not null, MOVIE_ID integer not null);";
		
		//create a table to store stats
		String statsTable = "CREATE TABLE stats (CORRECT integer, INCORRECT integer, TOTAL_TIME real);";
		db.execSQL(moviesTable);
		db.execSQL(starsTable);
		db.execSQL(starsInMoviesTable);
		db.execSQL(statsTable);
		
		// populate database
		try {
//			BufferedReader in = new BufferedReader(new InputStreamReader(mContext.getAssets().open(FILE_NAME)));
			BufferedReader in = new BufferedReader(new InputStreamReader(mContext.getAssets().open("movies.csv")));
			String line;

			System.out.println("HELLLOOOOO");
			while((line=in.readLine())!=null) {
				String fields[] = line.split("\\s*,\\s*");
				ContentValues values = new ContentValues();
				values.put("ID", fields[0].replace("\"", ""));
				values.put("TITLE", fields[1].replace("\"", ""));
				values.put("YEAR", fields[2].replace("\"", ""));
				values.put("DIRECTOR", fields[3].replace("\"", ""));
				db.insert("movies", null, values); 
			}
			
			in = new BufferedReader(new InputStreamReader(mContext.getAssets().open("stars.csv")));
			
			while((line=in.readLine())!=null) {
				String fields[] = line.split("\\s*,\\s*");
				ContentValues values = new ContentValues();
				values.put("ID", fields[0].replace("\"", ""));
				values.put("FIRST_NAME", fields[1].replace("\"", ""));
				values.put("LAST_NAME", fields[2].replace("\"", ""));
				values.put("DOB", fields[3].replace("\"", ""));
				db.insert("stars", null, values); 
			}
			
			in = new BufferedReader(new InputStreamReader(mContext.getAssets().open("stars_in_movies.csv")));
			while((line=in.readLine())!=null) {
				String fields[] = line.split("\\s*,\\s*");
				ContentValues values = new ContentValues();
				values.put("STAR_ID", fields[0].replace("\"", ""));
				values.put("MOVIE_ID", fields[1].replace("\"", ""));
				db.insert("stars_in_movies", null, values); 
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS movies");
		db.execSQL("DROP TABLE IF EXISTS stars");
		db.execSQL("DROP TABLE IF EXISTS stars_in_movies");
		db.execSQL("DROP TABLE IF EXISTS stats");
		onCreate(db);
	}
	
	public Cursor fetchAll() {
		return mDb.query("movies", new String[] {"TITLE"}, null, null, null, null, null);
	}


    public Question getQuestion(int num)
    {
        Cursor cur;
        Question question = new Question();
        ArrayList<Answer> ansList = new ArrayList<Answer>();
        ArrayList<String> starIds = new ArrayList<String>();
        String movieId, questionString;

        switch(num) {
            case 0: //Who directed the movie X?
                cur = mDb.rawQuery("SELECT DISTINCT title,director " +
                        "FROM movies " +
                        "WHERE title IS NOT NULL AND director IS NOT NULL " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 4", null);

                //get our answer
                cur.moveToFirst();
                question.setString("Who directed \"" + cur.getString(0) + "\"?");
                ansList.add(new Answer(cur.getString(1), true));

                //get our wrong answers
                // int i = 1; i < size; i++
                for (cur.moveToNext(); !cur.isAfterLast(); cur.moveToNext()) {
                    ansList.add(new Answer(cur.getString(1), false));
                }
                cur.close();
                //shuffle our list
                Collections.shuffle(ansList);
                question.setAnswers(ansList);
                break;

            case 1: //When was the movie X released?
                cur = mDb.rawQuery("SELECT DISTINCT title, year " +
                        "FROM movies " +
                        "WHERE title IS NOT NULL AND year IS NOT NULL " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 4", null);

                //get our answer
                cur.moveToFirst();
                question.setString("When was \"" + cur.getString(0) + "\" released?");
                ansList.add(new Answer(cur.getString(1), true));

                //get our wrong answers
                // int i = 1; i < size; i++
                for (cur.moveToNext(); !cur.isAfterLast(); cur.moveToNext()) {
                    ansList.add(new Answer(cur.getString(1), false));
                }
                cur.close();
                //shuffle our list
                Collections.shuffle(ansList);
                question.setAnswers(ansList);
                break;

            case 2: //which star was/was not in the movie X?
                if (Math.random() < 0.5) //which star WAS in movie X?
                {
                    //get a movie and a star
                    cur = mDb.rawQuery("SELECT stars.first_name, stars.last_name, movies.title, movies.id " +
                            "FROM stars, movies, stars_in_movies " +
                            "WHERE stars.id = stars_in_movies.star_id AND movies.id = stars_in_movies.movie_id " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 1", null);
                    cur.moveToFirst();
                    //get question
                    question.setString("Which star was in \"" + cur.getString(2) + "\"?");
                    //get correct answer (first_last) of star's name
                    ansList.add(new Answer(cur.getString(0) + " " + cur.getString(1), true));

                    //keep our movieId for later
                    movieId = cur.getString(3);
                    cur.close();
                    //get 3 stars that were not in that movie
                    cur = mDb.rawQuery("SELECT DISTINCT stars.first_name, stars.last_name " +
                            "FROM stars, stars_in_movies " +
                            "WHERE stars_in_movies.movie_id != " + movieId + " AND stars.id = stars_in_movies.star_id " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 3", null);
                    for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                        ansList.add(new Answer(cur.getString(0) + " " + cur.getString(1), false));
                    }
                    cur.close();
                    Collections.shuffle(ansList);
                    question.setAnswers(ansList);
                } else //which star WAS NOT in movie X?
                {
                    //get a movie with at least 3 stars
                    cur = mDb.rawQuery("SELECT m.title, m.id " +
                            "FROM stars_in_movies as sim, movies as m " +
                            "WHERE m.id = sim.movie_id " +
                            "GROUP BY sim.movie_id " +
                            "HAVING COUNT(*) > 2 " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 1", null);


                    cur.moveToFirst();
                    //get question
                    question.setString("Which star was NOT in \"" + cur.getString(0) + "\"?");
                    //keep movieId for later
                    movieId = cur.getString(1);
                    cur.close();
                    //get 3 stars from that movie
                    cur = mDb.rawQuery("SELECT DISTINCT s.first_name, s.last_name, s.id " +
                            "FROM stars as s, stars_in_movies as sim " +
                            "WHERE s.id = sim.star_id AND sim.movie_id = " + movieId + " " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 3", null);


                    for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                        ansList.add(new Answer(cur.getString(0) + " " + cur.getString(1), false));
                        starIds.add(cur.getString(2));
                    }
                    cur.close();
                    //get a star not in that movie
                    cur = mDb.rawQuery("SELECT s.first_name, s.last_name " +
                            "FROM stars as s, stars_in_movies as sim " +
                            "WHERE s.id = sim.star_id AND sim.movie_id != " + movieId + " AND s.id != " + starIds.get(0) + " AND s.id != " + starIds.get(1) + " AND s.id != " + starIds.get(2) + " " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 1", null);
                    cur.moveToFirst();
                    //get our correct answer
                    ansList.add(new Answer(cur.getString(0) + " " + cur.getString(1), true));

                    cur.close();
                    Collections.shuffle(ansList);
                    question.setAnswers(ansList);
                }
                break;

            case 3: //In which movies did the stars X and Y appear together?
                //get a movie with atleast 2 stars, which is our answer
                cur = mDb.rawQuery("SELECT m.title, m.id " +
                        "FROM stars_in_movies as sim, movies as m " +
                        "WHERE m.id = sim.movie_id " +
                        "GROUP BY sim.movie_id " +
                        "HAVING COUNT(*) > 1 " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 1", null);
                //get our id for later
                cur.moveToFirst();
                movieId = cur.getString(1);
                //this is our answer
                ansList.add(new Answer(cur.getString(0), true));

                cur.close();
                //get our 2 stars
                cur = mDb.rawQuery("SELECT DISTINCT s.id, s.first_name, s.last_name " +
                        "FROM stars as s, stars_in_movies as sim " +
                        "WHERE s.id = sim.star_id AND sim.movie_id != " + movieId + " " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 2", null);

                cur.moveToFirst();
                starIds.add(cur.getString(0)); //get a string for 3rd query

                //construct our question
                questionString = "In what movie did " + cur.getString(1) + " " + cur.getString(2) + " and ";
                cur.moveToNext();
                questionString += cur.getString(1) + " " + cur.getString(2) + " appear together?";
                question.setString(questionString);

                cur.close();
                //get our 3 incorrect movie answers
                cur = mDb.rawQuery("SELECT DISTINCT m.title " +
                        "FROM movies as m, stars_in_movies as sim " +
                        "WHERE m.id = sim.movie_id AND m.id != " + movieId + " AND sim.star_id != " + starIds.get(0) + " " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 3", null);

                for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext())
                {
                    ansList.add(new Answer(cur.getString(0),false));
                }
                cur.close();
                Collections.shuffle(ansList);
                question.setAnswers(ansList);

                break;

            case 4: //Who directed/did not direct the star X?
                //replace with (Math.random() < 0.5) to enable coin flip
                if (Math.random() < 0.0) //who DID direct star X?
                {
                    cur = mDb.rawQuery("SELECT m.director, s.first_name, s.last_name, s.id " +
                            "FROM movies as m, stars_in_movies as sim, stars as s " +
                            "WHERE m.id = sim.movie_ID AND s.id = sim.star_id " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 1",null);
                    cur.moveToFirst();
                    question.setString("Who has directed " + cur.getString(1) + " " + cur.getString(2) + "?");

                    //values for next query

                    String director = cur.getString(0);
                    starIds.add(cur.getString(3));
                    //our answer
                    ansList.add(new Answer(director,true));

                    cur.close();
                    cur = mDb.rawQuery("SELECT m.director " +
                            "FROM movies as m, stars_in_movies as sim, stars as s " +
                            "WHERE s.id != " + starIds.get(0) + " AND s.id = sim.star_id AND m.id = sim.movie_id AND m.director != \'" + director + "\' " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 3",null);

                    //get our wrong answers
                    for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext())
                    {
                        ansList.add(new Answer(cur.getString(0),false));
                    }
                    cur.close();

                    Collections.shuffle(ansList);
                    question.setAnswers(ansList);

                }
                else //who DID NOT direct the star X?
                {
                    //get a random movie with a star
                    cur = mDb.rawQuery("SELECT m.director, s.first_name, s.last_name, s.id " +
                            "FROM movies as m, stars_in_movies as sim, stars as s " +
                            "WHERE m.id = sim.movie_id AND s.id = sim.star_id AND m.director IS NOT NULL " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 1",null);
                    cur.moveToFirst();
                    String director = cur.getString(0); //get our director
                    String starId = cur.getString(3);
                    question.setString("Who has NOT directed " + cur.getString(1) + " " + cur.getString(2) + "?");
                    ansList.add(new Answer(director,true));

                    cur.close();
                    //get 3 directors that did not direct that star
                    cur =mDb.rawQuery("SELECT DISTINCT director " +
                            "FROM movies " +
                            "WHERE director NOT IN " +
                            "(SELECT DISTINCT m.director " +
                            "FROM movies as m, stars_in_movies as sim, stars as s " +
                            "WHERE m.id = sim.movie_id AND s.id = sim.star_id AND s.id = " + starId + ") " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 3", null);

                    for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext())
                    {
                        ansList.add(new Answer(cur.getString(0),false));
                    }
                    cur.close();
                    Collections.shuffle(ansList);
                    question.setAnswers(ansList);

                }
                break;

            case 5: //Which star appears in both movies X and Y?
                cur = mDb.rawQuery("SELECT s.id, s.first_name, s.last_name " +
                        "FROM stars_in_movies as sim, stars as s " +
                        "WHERE s.id = sim.star_id " +
                        "GROUP BY sim.star_id " +
                        "HAVING COUNT(*) > 1 " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 1",null);
                cur.moveToFirst();
                ansList.add(new Answer(cur.getString(1) + " " + cur.getString(2), true));
                starIds.add(cur.getString(0)); //save id for later
                cur.close();
                cur = mDb.rawQuery("SELECT m.title " +
                        "FROM stars_in_movies as sim, movies as m " +
                        "WHERE m.id = sim.movie_id AND sim.star_id = " + starIds.get(0) + " " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 2",null);

                cur.moveToFirst();
                questionString = "Which star appears in both \"" + cur.getString(0) + "\" and \"";
                cur.moveToNext();
                questionString += cur.getString(0) + "\"?";
                question.setString(questionString);

                cur.close();
                cur = mDb.rawQuery("SELECT s.first_name, s.last_name " +
                        "FROM stars_in_movies as sim, stars as s " +
                        "WHERE s.id = sim.star_id " +
                        "GROUP BY sim.star_id " +
                        "HAVING COUNT(*) < 2 " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 3",null);

                for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext())
                {
                    ansList.add(new Answer(cur.getString(0) + " " + cur.getString(1),false));
                }
                cur.close();

                Collections.shuffle(ansList);
                question.setAnswers(ansList);

                break;

            case 6: //Which star did not appear in the same movie with the star X?
                //get a movie with 4 stars, guarantees 3 wrong answers
                cur = mDb.rawQuery("SELECT m.id " +
                        "FROM stars_in_movies as sim, movies as m " +
                        "WHERE m.id = sim.movie_id " +
                        "GROUP BY sim.movie_id " +
                        "HAVING COUNT(*) > 3 " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 1", null);
                //get our id for later
                cur.moveToFirst();
                movieId = cur.getString(0);

                cur.close();
                //get our star
                cur = mDb.rawQuery("SELECT DISTINCT s.id, s.first_name, s.last_name " +
                        "FROM stars as s, stars_in_movies as sim " +
                        "WHERE s.id = sim.star_id AND sim.movie_id != " + movieId + " " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 4", null);
                cur.moveToFirst();
                question.setString("Which star did NOT appear in a movie with " + cur.getString(1) + " " + cur.getString(2) + "?");
                starIds.add(cur.getString(0)); //use this ID to create or exclusion list

                cur.moveToNext(); //move up one
                //populate our wrong answers
                for(;!cur.isAfterLast(); cur.moveToNext())
                {
                    ansList.add(new Answer(cur.getString(1) + " " + cur.getString(2),false));
                }
                cur.close();

                //get a star that was not in a movie with our first star
                cur = mDb.rawQuery("SELECT DISTINCT s.first_name, s.last_name " +
                        "FROM stars as s, stars_in_movies as sim " +
                        "WHERE s.id = sim.star_id AND s.id != " + starIds.get(0) + " AND sim.movie_id != " + movieId + " AND sim.movie_id NOT IN " +
                        "( " +
                        "SELECT DISTINCT sim.movie_id " +
                        "FROM stars as s, stars_in_movies as sim " +
                        "WHERE s.id = sim.star_id AND s.id = " + starIds.get(0) +
                        ") " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 1",null);
                cur.moveToFirst();
                ansList.add(new Answer(cur.getString(0) + " " + cur.getString(1), true));
                cur.close();

                Collections.shuffle(ansList);
                question.setAnswers(ansList);

                break;

            case 7: // who directed the star X in year Y?
                //get a director, year and star
                cur = mDb.rawQuery("SELECT m.director, m.year, s.first_name, s.last_name, s.id " +
                        "FROM movies as m, stars_in_movies as sim, stars as s " +
                        "WHERE s.id = sim.star_id AND m.id = sim.movie_id " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 1",null);
                cur.moveToFirst();
                //our question
                question.setString("Who directed " + cur.getString(2) + " " + cur.getString(3) + " in " + cur.getString(1) + "?");
                movieId = cur.getString(1); //steal movieId var to save year
                starIds.add(cur.getString(4)); //get our star id
                ansList.add(new Answer(cur.getString(0),true));

                cur.close();
                //get 3 directors that did not direct our star in that year
                cur = mDb.rawQuery("SELECT DISTINCT director " +
                        "FROM movies " +
                        "WHERE director NOT IN " +
                        "( " +
                        "SELECT DISTINCT m.director " +
                        "FROM movies as m, stars_in_movies as sim, stars as s " +
                        "WHERE s.id = sim.star_id AND m.id = sim.movie_id AND s.id = " + starIds.get(0) + " AND m.year = " + movieId +
                        ") " +
                        "ORDER BY RANDOM() " +
                        "LIMIT 3", null);
                for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext())
                {
                    ansList.add(new Answer(cur.getString(0),false));
                }
                cur.close();

                Collections.shuffle(ansList);
                question.setAnswers(ansList);

                break;

            default:
                if (true)
                    return getQuestion(0); //DEFAULT, OUT OF BOUNDS
                break;
        }

        return question;
    }

	public void insertNewStats(int newCorrect, int newIncorrect, double newTimePerQuestion){
		ContentValues values = new ContentValues();
		values.put("CORRECT", newCorrect);
		values.put("INCORRECT", newIncorrect);
		values.put("TOTAL_TIME", newTimePerQuestion);
		mDb.insert("stats", null, values); 
	}
	
	public Cursor getStats(){
		Cursor cur;
		cur = mDb.rawQuery("SELECT * FROM stats", null);
		return cur;
	}
	
	public int getTotalNumQuizzes(){
		Cursor cur;
		cur = mDb.rawQuery("SELECT count(*) FROM stats", null);
		cur.moveToFirst();
		return cur.getInt(0);
	}
	
	public int getTotalNumCorrect(){
		Cursor cur;
		cur = mDb.rawQuery("SELECT stats.correct FROM stats", null);
		cur.moveToFirst();
		int total = 0;
		while(!cur.isAfterLast()){
			total += cur.getInt(0);
			cur.moveToNext();
		}
		return total;
	}
	
	public int getTotalNumIncorrect(){
		Cursor cur;
		cur = mDb.rawQuery("SELECT stats.incorrect FROM stats", null);
		cur.moveToFirst();
		int total = 0;
		while(!cur.isAfterLast()){
			total += cur.getInt(0);
			cur.moveToNext();
		}
		return total;
	}
	
	public double getTotalTime(){
		Cursor cur;
		cur = mDb.rawQuery("SELECT stats.total_time FROM stats", null);
		cur.moveToFirst();
		double total = 0;
		while(!cur.isAfterLast()){
			total += cur.getDouble(0);
			cur.moveToNext();
		}
		return total;
	}
	
	public void clearScores(){
		mDb.delete("stats", null, null);
	}
	

}
