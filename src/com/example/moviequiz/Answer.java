package com.example.moviequiz;

/**
 * Created by envison on 3/15/15.
 */
public class Answer {

    private String answerString;
    boolean flag;

    public Answer()
    {
        this.answerString = "NULL";
        this.flag = false;
    }

    public Answer(String ans, boolean corr)
    {
        this.answerString = ans;
        this.flag = corr;
    }

    public String getString() { return answerString; }

    public void setString(String str) { this.answerString = str; }

    public boolean isCorrect() { return flag; }

    public void setFlag(boolean b) { this.flag = b; }

}
